var services = angular.module('Services', []);

var global_total_images = 0;
var global_downloaded_images = 0;
var global_json_events = null;

var global_id_event = window.localStorage.getItem("id_event");
var domain =  window.localStorage.getItem("domain");
var ssid = window.localStorage.getItem("ssid");
var password =  window.localStorage.getItem("password");


function isOnline(){
   if (navigator.connection.type == Connection.UNKNOWN || navigator.connection.type == Connection.NONE ){
   	return false;
   }else{
   	return true;
   }
}

services.factory('DataService',function($http){
	
	function getData(callback, callback_capa,callback_capa_origin,clean_images){

		function sucess(data){
			var images = [];

		    images_event = data.imagens;
		    // Guardando nome da capa
		    var capa = data.name_imagem_padrao_monitor;
		    data = data.proximos_eventos;
		     		
 			var origin_capa =  window.localStorage.getItem("origin_capa");

	 	    if(origin_capa == null || origin_capa != capa){
	 	      console.log("origin capa vai ser baixada!");
	 	      console.log("nome: "+capa);
	    	  window.localStorage.setItem("origin_capa",capa);
	    	  
	    	  // Removendo capa antiga
	    	  if(origin_capa != null) {
	    	  	remove_image_capa(window.localStorage.getItem("origin_capa_storage"));
	    	  }
	    	  // Baixando capa nova
		      download_capa(capa,function(uri){
		      	console.log("sucesso ao baixar");
		      	window.localStorage.setItem("origin_capa_storage",uri);
		      	callback_capa_origin(uri);
		      });

		    }else {
		       console.log("Apenas carregue a capa antiga.");
		       // Apenas carregue a capa antiga.		       
		       console.log(window.localStorage.getItem("origin_capa_storage"));
		       callback_capa_origin(window.localStorage.getItem("origin_capa_storage"));
		    }

		    // Limpando todas as imagens de capas antigas.
		    var old_images_event = JSON.parse(window.localStorage.getItem("capas"));
		    if(old_images_event != null){
			    for (var i = 0; i < old_images_event.length; i++) {
			    	remove_image_capa(old_images_event[i]);
			    };
			}

			old_images_event = [];
			window.localStorage.setItem("capas", JSON.stringify(old_images_event));

		    // Baixando todas a imagens de capa nova
		    for (var i = 0; i < images_event.length; i++) {
		    	global_total_images += 1;
		    	callback_capa(images_event[i]);
		    };

 			// Navegando em todos os eventos
		    for (var i = 0; i < data.length; i++) {

		    	// Todas as imagens do evento
		    	for (var j = 0; j < data[i].imagens.length; j++) {
		    		global_total_images += 1;

		    		images.push({
		    			uri: data[i].imagens[j],
		    			start_time: data[i].data_inicio,
		    			end_time: data[i].data_fim
		    		});
		    	};


		    };

		    if(images.length == 0){
		    	window.localStorage.setItem("data", JSON.stringify(images));
		    }
		    // callback das imagens padrões.
		    callback(images);
		}

	//	$http.get(domain + '/tv/proximos_eventos/'+global_id_event).
		$http.get('http://201.38.240.130:8000/tv/proximos_eventos/19/').
		  success(function(data, status, headers, config) {
		  	alert("sucess : "+data);
		    if(JSON.stringify(data) != JSON.stringify(global_json_events)){
		    	global_json_events = data;
		    	console.log("JSON baixado!");
		    	clean_images(function(){
		    	  sucess(data);
		    	});		        
		    }
		  }).
		  error(function(data, status, headers, config) {		    
alert("erro : "+data);
		  });
	}

	return {
      getData: getData
	};
  
});

services.factory('FileService', function(DataService) {

    var lista_imagens = [];

	var callback_function_online = function () {};



	function getJSON(callback_online,callback_offline, callback_capa_online,callback_capa_origin,clean_images) {

		// Caso esteja online limpa os arquivos antigos e baixa os novos.
		//_____________________________________
	    if (isOnline()) {
	    	console.log("online");
	    	DataService.getData(function(data){
				lista_imagens = data;
				getImages(callback_online);
			},
  	 		// Fazendo o download da capa das imagens.
  	 		//_________________________________
			function(capa_url){
				download_capa(capa_url,callback_capa_online);
			},
			callback_capa_origin,
			clean_images);
	    } else {
	      console.log("offline");
	      // Carregando lista de imagens do Storage.
	      //__________________________________
	      callback_offline();	      
	    }

		
	}

	// Método que realiza o dowsload das imagens e chama a função de callback.
	function download(image_object){
	  var downloadUrl = 'http://201.38.240.130:8000'+image_object.uri;
	  var name = '_' + Math.random().toString(36).substr(2, 9);
	  var relativeFilePath = ".images_hotel/"+name+".jpg";  // using an absolute path also does not work

	  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
	     var fileTransfer = new FileTransfer();
	     fileTransfer.download(
	        downloadUrl,

	        fileSystem.root.toURL() + '/' + relativeFilePath,

	        function (entry) {
	          image_object.uri = entry.toURL();
	          callback_function_online(image_object);
	        },
	        function (error) {
	           global_total_images -= 1;
	           //alert("Error during download. Code = " + error.code);
	        }
	     );
	  });
	}

	// Método para apagar as ultimas imagens que estão guardadas no storage.
	function cleanImages(images_path){

		function removefile(file){
		  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){ 

			  fileSystem.root.getFile(".images_hotel/"+file.split('/').pop(), {create: false , exclusive: false}, gotRemoveFileEntry, fail);

			  function gotRemoveFileEntry(fileEntry){
			    fileEntry.remove(success, fail);
			  }

			  function success(entry) {
			    //alert("Removal succeeded");
			  }

			  function fail(error) {
			    //alert("Error removing file: " + error.code);
			  }

		  },function(){ 
		    //alert("error"); 
		  });
		}

	  for (var i = 0; i < images_path.length; i++) {
	    removefile(images_path[i].uri);
	  };
	}

  var getImages = function(callback_online){
    
    callback_function_online = callback_online;

  	// Apagando imagens do aparelho.
  	var images_clean = JSON.parse(window.localStorage.getItem("data"));
	if ( images_clean != null ) {
		cleanImages(images_clean); 
	}
	// Baixando todas as imagens da lista.
	for(var i = 0; i < lista_imagens.length; i++ ){
	download(lista_imagens[i]);
	}      
   
  }

  // Funções exportadas.
  return {
    getImages: getJSON
  }
  
});

services.factory('ValidateImagesService', function() {

	function isValid(image_object){
		if (  moment().isBetween(image_object.start_time, image_object.end_time) ){
			return true;
		}else{
			return false;
		}

	}

	return {
		isValid: isValid
	}

});


/* CAPA ------------------ */

  	// Método que realiza o dowsload das imagens e chama a função de callback.
	function download_capa(capa_url, callback){
	  var downloadUrl = 'http://201.38.240.130:8000'+capa_url;
	  var name = '_' + Math.random().toString(36).substr(2, 9);
	  var relativeFilePath = ".images_hotel/"+name+".jpg";  // using an absolute path also does not work

	  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
	     var fileTransfer = new FileTransfer();
	     fileTransfer.download(
	        downloadUrl,

	        fileSystem.root.toURL() + '/' + relativeFilePath,

	        function (entry) {       
	           callback(entry.toURL());
	        },
	        function (error) {
	        	global_total_images -= 1;
	          // alert("Error during download. Code = " + error.code);
	        }
	     );
	  });
	}

	function remove_image_capa(file){
		  window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem){ 

			  fileSystem.root.getFile(".images_hotel/"+file.split('/').pop(), {create: false , exclusive: false}, gotRemoveFileEntry, fail);

			  function gotRemoveFileEntry(fileEntry){
			    fileEntry.remove(success, fail);
			  }

			  function success(entry) {
			    //alert("Removal succeeded");
			  }

			  function fail(error) {
			    //alert("Error removing file: " + error.code);
			  }

		  },function(){ 
		    //alert("error"); 
		  });
	}