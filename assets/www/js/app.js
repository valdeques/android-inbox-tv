// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var app = angular.module('starter', ['ionic', 'Services']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});


app.run(function($ionicPlatform, $state) {
  $ionicPlatform.registerBackButtonAction(function(event) {

  }, 100);
})


app.controller('Controller', function($scope, $http, $ionicSlideBoxDelegate, $timeout, FileService, ValidateImagesService) {

  // Array de imagens para o slide.
  $scope.images = [];
  $scope.images_json = [];
  // Contar quantas imagens de eventos foram baixadas.
  var images_event = 0;

  $scope.capa = null;
  // Array das imagens do monitor não relacionadas a eventos.
  var capa_images = [];

  if (global_id_event == null || domain == null || ssid == null || password == null) {
    //$scope.menu = true;
  } else {
    $scope.menu = null;

    window.InboxApp.getConnection(ssid, password);

    $timeout(function() {
      FileService.getImages(
        callback_online,
        callback_offline,
        callback_capa_online,
        callback_capa_origin,
        continue_start
      );
    }, 2000);
  }

  $scope.id_event = global_id_event;
  $scope.domain = domain;
  $scope.ssid = ssid;

  $scope.save = function() {

    if (document.getElementById("id-input").value.length > 1) {
      global_id_event = document.getElementById("id-input").value;
      window.localStorage.setItem("id_event", global_id_event);
    }

    if (document.getElementById("domain-input").value.length > 1) {
      domain = document.getElementById("domain-input").value;
      window.localStorage.setItem("domain", domain);
    }

    $scope.menu = null;
    var ssid = document.getElementById("ssid-input").value;
    var password = document.getElementById("password-input").value;

    window.localStorage.setItem("ssid", ssid);
    window.localStorage.setItem("password", password);

    window.InboxApp.getConnection(ssid, password);

    $timeout(function() {
      FileService.getImages(
        callback_online,
        callback_offline,
        callback_capa_online,
        callback_capa_origin,
        continue_start
      );
    }, 2000);

  };

  var isM = false;
  var isE = false;
  var isN = false;

  window.onkeydown = function(e) {
    var keyCode = ('which' in event) ? event.which : event.keyCode;

    if (keyCode == 51) isM = true;

    if (isM == true && keyCode == 50) isE = true;
    if (isE == true && keyCode == 49) isN = true;

    if (isM == true && isE == true && isN == true && keyCode == 48) {
      $scope.menu = true;
      isM = false;
      isE = false;
      isN = false;
    }

    if (keyCode != 51 && keyCode != 50 && keyCode != 49 && keyCode != 48) {
      isM = false;
      isE = false;
      isN = false;
    }

  };

  document.addEventListener("deviceready", onDeviceReady, false);
  document.addEventListener("online", function() {
    document.getElementById("net-status").innerHTML = "<div class=\"circle-online\"></div>";
    console.log("event online");
  }, false);

  document.addEventListener("offline", function() {
    document.getElementById("net-status").innerHTML = "<div class=\"circle-offline\"></div>";
    console.log("event offline");
  }, false);

  // Quando os plugins do phonegap estiverem prontos.
  function onDeviceReady() {
    StatusBar.hide();
    ajusteDeHora();

    function ajusteDeHora() {
      if (isOnline()) {
        $http.get('http://bani.appspot.com/hora.callback?callback=?').
        success(function(data, status, headers, config) {

          //var date = JSON.parse(data);
          var newdate = data.replace("?", "");
          newdate = newdate.replace('(', '');
          newdate = newdate.replace(')', '');
          var dataAtual = moment(newdate, "DD/MM/YYYY").format("YYYY/MM/DD");
          dataAtual = dataAtual.replace('/', '');
          dataAtual = dataAtual.replace('/', '');
          var horaAtual = JSON.parse(newdate).hour;
          horaAtual = horaAtual + JSON.parse(newdate).minute + "00";
          window.InboxApp.ajusteDeHora(dataAtual, horaAtual);

        }).
        error(function(data, status, headers, config) {

        });

      }
    }

    // Conseguindo as imagens do service(Factory).

  }

  // Bloqueia o touch e o mouse.
  //___________________________
  $scope.slidestop = function(index) {
    $ionicSlideBoxDelegate.enableSlide(false);
  }

  // Caso esteja online.
  //___________________________
  function callback_online(image_object) {

    global_downloaded_images += 1;
    images_event += 1;

    if (global_downloaded_images == global_total_images) {
      console.log("total de imagens : " + global_total_images);
      console.log("total de imagens baixadas: " + global_downloaded_images);
      intervalManageImages();
    }

    $scope.$apply(function() {

      if (ValidateImagesService.isValid(image_object)) {
        if ($scope.capa != null) {
          $scope.images = [];
          $scope.capa = null;
        };

        console.log("Adicionando imagem de evento");
        console.log(image_object.uri);
        $scope.images.push(image_object.uri);
        console.log("slide: " + $scope.images.length);
      }

      $scope.images_json.push(image_object);
      // Salvando a lista de imagens salvas no storage.
      window.localStorage.setItem("data", JSON.stringify($scope.images_json));
    });
  }


  // Caso esteja offline.
  //___________________________
  function callback_offline() {

    $scope.$apply(function() {
      // CALLBACK OFFLINE

      $scope.images = [];
      console.log("Array limpo >< callback off inicio");
      $scope.images_json = JSON.parse(window.localStorage.getItem("data"));

      $scope.capa = true;
      callback_capa_origin(window.localStorage.getItem("origin_capa"));
      var capas = JSON.parse(window.localStorage.getItem("capas"));


      // Adicionando imagens de capa ao slide.
      if (capas != null) {
        for (var i = 0; i < capas.length; i++) {
          console.log("Adicionando " + capas[i]);
          $scope.images.push(capas[i]);
        };
      }

      if ($scope.images_json != null) {
        for (var i = 0; i < $scope.images_json.length; i++) {
          if (ValidateImagesService.isValid($scope.images_json[i])) {

            // Limpando imagens de capa que estejam no slide.
            if ($scope.capa != null) {
              $scope.images = [];
              console.log("Array limpo >< callback offline");
              $scope.capa = null;
            };

            console.log("adicionando imagem evento");
            console.log($scope.images_json[i].uri);
            $scope.images.push($scope.images_json[i].uri);
          }
        };

        intervalManageImages();
      }

    });
  }

  // Callback da capa online.
  //_____________________________
  function callback_capa_online(uri) {
    global_downloaded_images += 1;

    // Caso todas as imagens estejam baixadas iniciar o gerenciamento temporal delas.      
    if (global_downloaded_images == global_total_images) {
      intervalManageImages();
    }


    // Adicionando endereço no array de imagens com capas para serem guardadas.    
    capa_images.push(uri);

    window.localStorage.setItem("capas", JSON.stringify(capa_images));

    // Caso não exista nenhuma imagem de evento no slide adicionar essa imagem de capa ao slide.
    if (images_event == 0) {

      /* Avisando para o callback das imagens de evento 
       que existem imagens de capa executando no slide*/
      $scope.$apply(function() {
        $scope.capa = true;
        $scope.images.push(uri);
      });

      $ionicSlideBoxDelegate.update();
    };
  }

  function callback_capa_origin(uri) {
    if (images_event == 0) {
      $scope.capa = true;
      $scope.images.push(uri);
    }
  }

  function clean_images(start) {
    $scope.capa = null;
    images_event = 0;
    $scope.images = [];
    $scope.images_json = [];
    capa_images = [];
    console.log("Array limpo >< clean images");
    global_total_images = 0;
    global_downloaded_images = 0;

    start();
  }

  function continue_start(start) {
    start();
  }


  // Funções para controlar a animação do slide.
  $scope.next = function() {
    $ionicSlideBoxDelegate.update();
    $ionicSlideBoxDelegate.enableSlide();
    var current = $ionicSlideBoxDelegate.currentIndex();
    var count = $scope.images.length - 1;
    if (current == count) {
      $ionicSlideBoxDelegate.slide(0);
    } else {
      $ionicSlideBoxDelegate.next();
    }
  };

  // Transição de slide, timeout.
  var intervalFunctionSlide = function() {
    $timeout(function() {
      $scope.next();
      intervalFunctionSlide();
    }, 5000);
  };


  function validateImages() {

  }

  var intervalManageImages = function() {
    $timeout(function() {
      //validateImages();

      FileService.getImages(
        callback_online,
        callback_offline,
        callback_capa_online,
        callback_capa_origin,
        clean_images
      );

      intervalManageImages();
    }, 60000);

  };


  // chamando pela primeira vez.
  intervalFunctionSlide();

  //intervalManageImages();

  function tamanho() {
    $timeout(function() {
      console.log("slide: " + $scope.images.length);
      tamanho();
    }, 1000);
  }



});