package com.ionicframework.imagem386486;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.webkit.JavascriptInterface;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by valdeques on 02/04/15.
 */
public class InboxApp {

    private CordovaApp mGap;

    public InboxApp(CordovaApp gap)
    {
        mGap = gap;
    }


    @JavascriptInterface
    public void getConnection(String ssid, String password){

        WifiConfiguration wfc = new WifiConfiguration();

        wfc.SSID = "\"".concat(ssid).concat("\"");
        wfc.status = WifiConfiguration.Status.DISABLED;
        wfc.priority = 40;

        wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);


        wfc.preSharedKey = "\"".concat(password).concat("\"");

        WifiManager wfMgr = (WifiManager) mGap.getSystemService(Context.WIFI_SERVICE);
        int networkId = wfMgr.addNetwork(wfc);

        if (networkId != -1) {
            // success, can call wfMgr.enableNetwork(networkId, true) to connect
            wfMgr.enableNetwork(networkId, true);
        }
    }

    @JavascriptInterface
    public void ajusteDeHora(String data,String time){
        try {
            Process process = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(process.getOutputStream());
            String command = "date -s "+data+"."+time+"\n";
            Log.e("command", command);
            os.writeBytes(command);
            os.flush();
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
