package com.ionicframework.imagem386486;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by valdeques on 22/04/15.
 */
public class AutoStart extends BroadcastReceiver {

      public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving

        // Start Service On Boot Start Up
        Intent service = new Intent(context, TestService.class);
        context.startService(service);

        //Start App On Boot Start Up
        Intent App = new Intent(context, CordovaApp.class);
        App.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(App);


    }
}